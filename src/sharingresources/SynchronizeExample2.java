package sharingresources;

public class SynchronizeExample2 {

    public static void main(String[] args) throws InterruptedException {

        Counter counter = new Counter();

        IncrementThread incrementThread = new IncrementThread(counter);
        DecrementThread decrementThread = new DecrementThread(counter);

        incrementThread.start();
        decrementThread.start();

        incrementThread.join();
        decrementThread.join();

        System.out.println("Result is " + counter.getCounter());
    }

    public static class DecrementThread extends Thread {

        private Counter counter;

        DecrementThread(Counter counter) {
            this.counter = counter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                counter.decrement();
            }
        }
    }

    public static class IncrementThread extends Thread {

        private Counter counter;

        IncrementThread(Counter counter) {
            this.counter = counter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                counter.increment();
            }
        }
    }

    public static class Counter {

        private int counter = 0;

        Object lock = new Object();

        public  void increment() {

            synchronized (lock) {
                this.counter++;
            }
        }

        public  void decrement() {
            synchronized (lock) {
                this.counter--;
            }
        }

        public  int getCounter() {
            synchronized (lock) {
                return counter;
            }
        }
    }

}
