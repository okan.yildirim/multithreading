package sharingresources;

import java.util.Random;

public class CapturingMetricUseCase {

    public static void main(String[] args) {
        Metric metric = new Metric();

        BusinessLogic businessLogic1 = new BusinessLogic(metric);
        BusinessLogic businessLogic2 = new BusinessLogic(metric);

        MetricPrinter metricPrinter = new MetricPrinter(metric);

        businessLogic1.start();
        businessLogic2.start();
        metricPrinter.start();
    }

    static class MetricPrinter extends Thread {

        private Metric metric;

        MetricPrinter(Metric metric) {
            this.metric = metric;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {

                }
                System.out.println("Average: " + metric.getAverage());
            }
        }
    }


    static class BusinessLogic extends Thread {

        private Metric metric;
        Random random = new Random();

        BusinessLogic(Metric metric) {
            this.metric = metric;
        }

        @Override
        public void run() {

            while (true) {
                long start = System.currentTimeMillis();
                try {
                    Thread.sleep(random.nextInt(10));
                } catch (InterruptedException e) {
                }
                long end = System.currentTimeMillis();

                metric.addSample(end - start);
            }
        }
    }

    static class Metric {
        private long count;
        private volatile double average;

        public synchronized void addSample(long sample) {
            double currentSum = average * count;
            count++;
            average = (currentSum + sample) / count;
        }

        public double getAverage() {
            return average;
        }
    }
}
