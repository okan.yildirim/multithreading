public class ExceptionHandlingExample {

    public static void main(String[] args) {

        Thread thread = new Thread(() -> {

            throw new RuntimeException("Intentional Exception");
        });

        thread.setName("Misbehaving thread");

        thread.setUncaughtExceptionHandler((thread1, throwable) -> {
            System.out.println("A critical error happened in thread " +  thread1.getName() +
                    " and the error is" + throwable.getMessage());
        });

        thread.start();
    }
}
