package sharingresources;

public class MinMaxSampleExercise {

    public class MinMaxMetrics {

        // Add all necessary member variables
        private volatile long min ;
        private volatile long max ;

        /**
         * Initializes all member variables
         */
        public MinMaxMetrics() {
            // Add code here
            this.min = 0L;
            this.max = 0L;
        }

        /**
         * Adds a new sample to our metrics.
         */
        public synchronized void addSample(long newSample) {
            // Add code here
            if (newSample < this.min){
                this.min = newSample;
            } else if (newSample > this.max){
                this.max = newSample;
            }
        }

        /**
         * Returns the smallest sample we've seen so far.
         */
        public long getMin() {
            // Add code here
            return this.min;
        }

        /**
         * Returns the biggest sample we've seen so far.
         */
        public long getMax() {
            return this.max;
            // Add code here
        }
    }




    ///////////////////////////////

    public class MinMaxMetricsSolution {

        private volatile long minValue;
        private volatile long maxValue;

        /**
         * Initializes all member variables
         */
        public MinMaxMetricsSolution() {
            this.maxValue = Long.MIN_VALUE;
            this.minValue = Long.MAX_VALUE;
        }

        /**
         * Adds a new sample to our metrics.
         */
        public void addSample(long newSample) {
            synchronized (this) {
                this.minValue = Math.min(newSample, this.minValue);
                this.maxValue = Math.max(newSample, this.maxValue);
            }
        }

        /**
         * Returns the smallest sample we've seen so far.
         */
        public long getMin() {
            return this.minValue;
        }

        /**
         * Returns the biggest sample we've seen so far.
         */
        public long getMax() {
            return this.maxValue;
        }
    }

}
