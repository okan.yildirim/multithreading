package joiningthread;

import java.math.BigInteger;

public class HandsOnExample {

    public static void main(String[] args) throws InterruptedException {

        BigInteger result = calculateResult(BigInteger.valueOf(2), BigInteger.valueOf(4), BigInteger.valueOf(3), BigInteger.valueOf(2));

        System.out.println(result);
    }

    public static BigInteger calculateResult(BigInteger base1, BigInteger power1, BigInteger base2, BigInteger power2) throws InterruptedException {
        BigInteger result;
        /*
            Calculate result = ( base1 ^ power1 ) + (base2 ^ power2).
            Where each calculation in (..) is calculated on a different thread
        */

        PowerCalculatingThread thread1 = new PowerCalculatingThread(base1, power1);
        thread1.start();
        PowerCalculatingThread thread2 = new PowerCalculatingThread(base2, power2);
        thread2.start();
        thread1.join();
        thread1.join();

        result = thread1.getResult().add(thread2.getResult());
        return result;
    }

    private static class PowerCalculatingThread extends Thread {
        private BigInteger result = BigInteger.ONE;
        private BigInteger base;
        private BigInteger power;

        public PowerCalculatingThread(BigInteger base, BigInteger power) {
            this.base = base;
            this.power = power;
        }

        @Override
        public void run() {
           /*
           Implement the calculation of result = base ^ power
           */
            BigInteger result = BigInteger.ONE;
            for (BigInteger i = BigInteger.ZERO; i.compareTo(power) != 0; i = i.add(BigInteger.ONE)) {
                result = result.multiply(base);
            }
            this.result = result;
        }

        public BigInteger getResult() {
            return result;
        }
    }
}