package interrupt;

import java.math.BigInteger;

public class InterruptInLoop {

    public static void main(String[] args) {

        Thread thread = new Thread(new LongComputationTask(BigInteger.valueOf(25665L), BigInteger.valueOf(56666650)));
        thread.start();
        thread.interrupt();
    }


    private static class LongComputationTask implements Runnable {

        private BigInteger base;
        private BigInteger power;

        LongComputationTask(BigInteger base, BigInteger power) {
            this.base = base;
            this.power = power;
        }

        private BigInteger pow(BigInteger base, BigInteger power) {

            BigInteger result = BigInteger.ONE;
            for (BigInteger i = BigInteger.ZERO; i.compareTo(power) != 0; i = i.add(BigInteger.ONE)) {

                if (Thread.currentThread().isInterrupted()){
                    System.out.println("Premature interrupted computation");
                    return BigInteger.ZERO;
                }
                result = result.multiply(base);
            }

            return result;
        }

        @Override
        public void run() {

            System.out.println(base + "^" + power + "=" + pow(base, power));

        }
    }
}
