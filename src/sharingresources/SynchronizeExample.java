package sharingresources;

public class SynchronizeExample {

    public static void main(String[] args) throws InterruptedException {

        Counter counter = new Counter();

        IncrementThread incrementThread = new IncrementThread(counter);
        DecrementThread decrementThread = new DecrementThread(counter);

        incrementThread.start();
        decrementThread.start();

        incrementThread.join();
        decrementThread.join();

        System.out.println("Result is " + counter.getCounter());
    }

    public static class DecrementThread extends Thread {

        private Counter counter;

        DecrementThread(Counter counter) {
            this.counter = counter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                counter.decrement();
            }
        }
    }

    public static class IncrementThread extends Thread {

        private Counter counter;

        IncrementThread(Counter counter) {
            this.counter = counter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10000; i++) {
                counter.increment();
            }
        }
    }

    public static class Counter {

        private int counter = 0;


        public synchronized void increment() {
            this.counter++;
        }

        public synchronized void decrement() {
            this.counter--;
        }

        public synchronized int getCounter() {
            return counter;
        }
    }

}
