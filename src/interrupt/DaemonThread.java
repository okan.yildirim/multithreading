package interrupt;

import java.math.BigInteger;

public class DaemonThread {

    /*
    Daemon threads that do not prevent the application from existing if thr main thread terminated
    */
    public static void main(String[] args) {
        Thread thread = new Thread(new LongComputationTask2(BigInteger.valueOf(25665L), BigInteger.valueOf(56666650)));
        thread.setDaemon(true);
        thread.start();
        thread.interrupt();
    }

    private static class LongComputationTask2 implements Runnable {

        private BigInteger base;
        private BigInteger power;

        LongComputationTask2(BigInteger base, BigInteger power) {
            this.base = base;
            this.power = power;
        }

        private BigInteger pow(BigInteger base, BigInteger power) {

            BigInteger result = BigInteger.ONE;
            for (BigInteger i = BigInteger.ZERO; i.compareTo(power) != 0; i = i.add(BigInteger.ONE)) {
                result = result.multiply(base);
            }

            return result;
        }

        @Override
        public void run() {

            System.out.println(base + "^" + power + "=" + pow(base, power));

        }
    }
}
